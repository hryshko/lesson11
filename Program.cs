﻿using System.Collections;

namespace Lesson11;

class Program
{
    static void Main(string[] args)
    {
        // Task 0.1
        try
        {
            ArrayList list = new ArrayList();
            object s = list[18];
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.GetType());
        }

        // Task 0.2
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        dictionary.Add("1", "Sun");
        dictionary.Add("2", "Rain");
        dictionary.Add("3", "Fog");
        dictionary.Add("4", 44);
        dictionary.Add("5", 55);
        dictionary.Add("6", "Alice");
        dictionary.Add("7", "Ann");
        dictionary.Add("8", "Dami");
        dictionary.Add("9", 99);
        dictionary.Add("10", "Have a good day!:)");

        Console.WriteLine("My dictionary consists of:");
        foreach (var kvp in dictionary)
        {
            Console.WriteLine($"Key: {kvp.Key}, Value: {kvp.Value}");
        }
    }    
}